package commands

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCreate(t *testing.T) {
	got := Create()
	require.NotNil(t, got)
	require.Equal(t, "create", got.Name)
	require.Len(t, got.Flags, 4)
}
