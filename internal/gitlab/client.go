package gitlab

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"

	log "github.com/sirupsen/logrus"
)

const apiBaseURL = "/api/v4/"

// ErrorResponse expected from the API
type ErrorResponse struct {
	statusCode int
	Message    string `json:"message,omitempty"`
	// TODO the API returns different fields for some different status codes
	// https://gitlab.com/gitlab-org/release-cli/-/issues/18
	Err string `json:"error,omitempty"`
}

// Error implements the error interface. Wraps an error message from the API into an error type
func (er *ErrorResponse) Error() string {
	err := fmt.Sprintf("API Error Response status_code: %d message: %s", er.statusCode, er.Message)

	if er.Err != "" {
		return fmt.Sprintf("%s error %s", err, er.Err)
	}

	return err
}

// HTTPClient is an interface that describes the available actions of the client.
// Use http.Client during runtime.
// See mock_httpClient_test.go for a testing implementation
type HTTPClient interface {
	Do(*http.Request) (*http.Response, error)
}

// Client is used to send requests to the GitLab API. Normally created with the `New` function
type Client struct {
	baseURL    string
	token      string
	projectID  string
	httpClient HTTPClient
}

// New creates a new GitLab Client
func New(serverURL, token, projectID string, httpClient HTTPClient) (*Client, error) {
	u, err := url.Parse(serverURL)
	if err != nil {
		return nil, fmt.Errorf("failed to parse url: %w", err)
	}

	u.Path = path.Join(u.Path, apiBaseURL)

	return &Client{
		baseURL:    u.String(),
		token:      token,
		projectID:  projectID,
		httpClient: httpClient,
	}, nil
}

// request creates a new request and attaches
func (gc *Client) request(ctx context.Context, method, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, gc.baseURL+url, body)
	if err != nil {
		return nil, err
	}

	req = req.WithContext(ctx)

	// TODO support `PRIVATE-TOKEN` when the cli is not used in CI
	// https://gitlab.com/gitlab-org/release-cli/issues/11
	req.Header.Set("JOB-TOKEN", gc.token)
	req.Header.Set("Content-Type", "application/json")

	return req, nil
}

func checkClosed(closer io.Closer) {
	if err := closer.Close(); err != nil {
		log.WithError(err).Warn("failed to close")
	}
}
