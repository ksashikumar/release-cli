package main

import (
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/release-cli/internal/app"
)

var (
	// VERSION when the binary was built
	VERSION string
)

func main() {
	defer recovered()

	if err := app.New(VERSION).Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func recovered() {
	if r := recover(); r != nil {
		log.Fatalf("recovered from panic: %+v", r)
	}
}
