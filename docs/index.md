# GitLab Release command-line tool

> [Introduced](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/6) in GitLab 12.10.

GitLab Release command-line tool is an application written in [Golang](https://golang.org/)
to interact with [GitLab's Releases API](https://docs.gitlab.com/ee/api/releases/index.html) through the command line and through GitLab CI/CD's configuration file, `.gitlab-ci.yml`.

It consumes instructions in the `:release` node of the `.gitlab-ci.yml` to create a Release object in GitLab Rails.

The GitLab Release CLI is a decoupled utility that may be called by the GitLab Runner,
by a third-party CI or directly from the command line.
It uses the CI `Job-Token` to authorize against the GitLab Rails API, which is passed to it by the GitLab Runner.

The CLI can also be called independently, and can still create the Release via Rails API
if the `Job-Token` and correct command line params are provided.


```mermaid
sequenceDiagram
  participant Rails
  participant ReleaseCLI
  participant Runner
  Runner->>Rails: 1. Runner calls API for job info
  Rails->>Rails : 2. Yaml exposed as Steps
  Runner->>ReleaseCLI : 3. Runner calls CLI on Job success
  ReleaseCLI->>Rails : 4. CLI retrieves Release Steps
  ReleaseCLI->>Rails : 5. CLI creates Release
```

1. Runner calls API for job info: the Runner polls Rails for new Jobs.

1. Yaml exposed as **Steps**: The `release` node of the `.gitlab-ci.yml` configuration is converted into **Steps** and made available via API endpoint.

1. The Runner calls GitLab Release CLI: the Runner executes the job, and upon success calls the **GitLab Release**.

1. GitLab Release CLI retrieves Release **Steps**: the GitLab Release calls the Rails API to retrieve the `release` configuration (as **Steps**).

1. GitLab Release CLI creates a Release: the GitLab Release CLI makes an API call to Rails to create the new Release.

## Usage

To get started, open your project in a terminal and run `release-cli help`
for usage options. The output will be:

```shell
NAME:
   release-cli - A CLI tool that interacts with GitLab's Releases API

USAGE:
   help [global options] command [command options] [arguments...]

VERSION:
   0.0.1~beta.6.g1a38cd5

DESCRIPTION:

CLI tool that interacts with GitLab's Releases API https://docs.gitlab.com/ee/api/releases/.

All configuration flags will default to GitLab's CI predefined environment variables (https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).
To override these values, use the [GLOBAL OPTIONS].

Get started with release-cli https://gitlab.com/gitlab-org/release-cli.

AUTHOR:
   GitLab Inc. <support@gitlab.com>

COMMANDS:
   create   Create a Release using GitLab's Releases API https://docs.gitlab.com/ee/api/releases/#create-a-release
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --server-url value  The base URL of the GitLab instance, including protocol and port, for example https://gitlab.example.com:8080 [$CI_SERVER_URL]
   --job-token value   Job token used for authenticating with the GitLab Releases API [$CI_JOB_TOKEN]
   --project-id value  The current project's unique ID; used by GitLab CI internally [$CI_PROJECT_ID]
   --help, -h          Show help (default: false)
   --version, -v       Print the version (default: false)
```

### Create a new release

This command uses the [Create a Release](https://docs.gitlab.com/ee/api/releases/) API.

```shell
release-cli --server-url https://gitlab.com --job-token=SOME_JOB_TOKEN --project-id 12345 create help
```

The output is:

```shell
NAME:
   help create - Create a Release using GitLab's Releases API https://docs.gitlab.com/ee/api/releases/#create-a-release

USAGE:
   help create [command options] [arguments...]

OPTIONS:
   --name value         The release name
   --description value  The description of the release, you can use Markdown
   --tag-name value     The tag the release will be created from [$CI_COMMIT_TAG]
   --ref value          If tag_name doesn’t exist, the release will be created from ref; it can be a commit SHA, another tag name, or a branch name [$CI_COMMIT_SHA]
   --help, -h           Show help (default: false)
```

## Configuration

All configuration flags will default to [GitLab's CI predefined environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

To override these values, use the flags available under the `GLOBAL OPTIONS`.
For example, use the flags to create a release with a custom GitLab server URL.

```shell
release-cli -server-url https://gitlab.mydomain.com create -name "My Release" -description "This is a new release for my amazing tool"
```

## Using this tool in GitLab CI

The `release-cli` tool will be available when [the new `release` section becomes available in `.gitlab-ci.yml`](https://gitlab.com/groups/gitlab-org/-/epics/2510).

If you would like to try GitLab Release on your project, add the following `script` to your `.gitlab-ci.yml` file:
 
```yaml
release-branch:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli
  when: manual
  # We recommend the use of `except: tags` to prevent these pipelines
  # from running. See the notes section below for details.
  except:
    - tags
  script:
    - release-cli create --name release-branch-$CI_JOB_ID --description release-branch-$CI_COMMIT_REF_NAME-$CI_JOB_ID --tag-name job-$CI_JOB_ID --ref $CI_COMMIT_SHA
```

### Notes

- GitLab Release CLI is still under development. Please report any issues in
its project [issue tracker](https://gitlab.com/gitlab-org/release-cli/issues).

- A new pipeline will run when [a new tag is created](https://gitlab.com/gitlab-org/gitlab/issues/16290).
We recommend adding `except: tags` to your job to prevent these pipelines from
running concurrently with `release-cli`.
