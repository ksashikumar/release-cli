module gitlab.com/gitlab-org/release-cli

go 1.13

require (
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.2.2
	github.com/urfave/cli/v2 v2.1.1
	github.com/vektra/mockery v0.0.0-20181123154057-e78b021dcbb5
)
